(defproject cs-demo-parser "0.1.0-SNAPSHOT"
  :plugins [[lein-protobuf-minimal "0.4.1"]]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [commons-io/commons-io "2.5"]
                 [funcool/octet "1.0.1"]
                 [org.flatland/protobuf "0.8.1"]])
