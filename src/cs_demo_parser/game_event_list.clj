(ns cs-demo-parser.game-event-list)
(def data
  {:descriptors
   [{:name server_spawn
     :eventid 0
     :keys
     [{:name hostname, :type 1}
      {:name address, :type 1}
      {:name port, :type 4}
      {:name game, :type 1}
      {:name mapname, :type 1}
      {:name maxplayers, :type 3}
      {:name os, :type 1}
      {:name dedicated, :type 6}
      {:name official, :type 6}
      {:name password, :type 6}]}
    {:name server_pre_shutdown, :eventid 1, :keys [{:name reason, :type 1}]}
    {:name server_shutdown, :eventid 2, :keys [{:name reason, :type 1}]}
    {:name server_cvar
     :eventid 3
     :keys [{:name cvarname, :type 1} {:name cvarvalue, :type 1}]}
    {:name server_message, :eventid 4, :keys [{:name text, :type 1}]}
    {:name server_addban
     :eventid 5
     :keys
     [{:name name, :type 1}
      {:name userid, :type 4}
      {:name networkid, :type 1}
      {:name ip, :type 1}
      {:name duration, :type 1}
      {:name by, :type 1}
      {:name kicked, :type 6}]}
    {:name server_removeban
     :eventid 6
     :keys [{:name networkid, :type 1} {:name ip, :type 1} {:name by, :type 1}]}
    {:name player_connect
     :eventid 7
     :keys
     [{:name name, :type 1}
      {:name index, :type 5}
      {:name userid, :type 4}
      {:name networkid, :type 1}
      {:name address, :type 1}]}
    {:name player_info
     :eventid 8
     :keys
     [{:name name, :type 1}
      {:name index, :type 5}
      {:name userid, :type 4}
      {:name networkid, :type 1}
      {:name bot, :type 6}]}
    {:name player_disconnect
     :eventid 9
     :keys
     [{:name userid, :type 4}
      {:name reason, :type 1}
      {:name name, :type 1}
      {:name networkid, :type 1}]}
    {:name player_activate, :eventid 10, :keys [{:name userid, :type 4}]}
    {:name player_connect_full
     :eventid 11
     :keys [{:name userid, :type 4} {:name index, :type 5}]}
    {:name player_say
     :eventid 12
     :keys [{:name userid, :type 4} {:name text, :type 1}]}
    {:name cs_round_start_beep, :eventid 13}
    {:name cs_round_final_beep, :eventid 14}
    {:name round_time_warning, :eventid 15}
    {:name team_info
     :eventid 16
     :keys [{:name teamid, :type 5} {:name teamname, :type 1}]}
    {:name team_score
     :eventid 17
     :keys [{:name teamid, :type 5} {:name score, :type 4}]}
    {:name teamplay_broadcast_audio
     :eventid 18
     :keys [{:name team, :type 5} {:name sound, :type 1}]}
    {:name gameui_hidden, :eventid 19}
    {:name items_gifted
     :eventid 20
     :keys
     [{:name player, :type 5}
      {:name itemdef, :type 3}
      {:name numgifts, :type 5}
      {:name giftidx, :type 5}
      {:name accountid, :type 3}]}
    {:name player_team
     :eventid 21
     :keys
     [{:name userid, :type 4}
      {:name team, :type 5}
      {:name oldteam, :type 5}
      {:name disconnect, :type 6}
      {:name autoteam, :type 6}
      {:name silent, :type 6}
      {:name isbot, :type 6}]}
    {:name player_class
     :eventid 22
     :keys [{:name userid, :type 4} {:name class, :type 1}]}
    {:name player_death
     :eventid 23
     :keys
     [{:name userid, :type 4}
      {:name attacker, :type 4}
      {:name assister, :type 4}
      {:name weapon, :type 1}
      {:name weapon_itemid, :type 1}
      {:name weapon_fauxitemid, :type 1}
      {:name weapon_originalowner_xuid, :type 1}
      {:name headshot, :type 6}
      {:name dominated, :type 4}
      {:name revenge, :type 4}
      {:name penetrated, :type 4}
      {:name noreplay, :type 6}]}
    {:name player_hurt
     :eventid 24
     :keys
     [{:name userid, :type 4}
      {:name attacker, :type 4}
      {:name health, :type 5}
      {:name armor, :type 5}
      {:name weapon, :type 1}
      {:name dmg_health, :type 4}
      {:name dmg_armor, :type 5}
      {:name hitgroup, :type 5}]}
    {:name player_chat
     :eventid 25
     :keys
     [{:name teamonly, :type 6} {:name userid, :type 4} {:name text, :type 1}]}
    {:name player_score
     :eventid 26
     :keys
     [{:name userid, :type 4}
      {:name kills, :type 4}
      {:name deaths, :type 4}
      {:name score, :type 4}]}
    {:name player_spawn
     :eventid 27
     :keys [{:name userid, :type 4} {:name teamnum, :type 4}]}
    {:name player_shoot
     :eventid 28
     :keys
     [{:name userid, :type 4} {:name weapon, :type 5} {:name mode, :type 5}]}
    {:name player_use
     :eventid 29
     :keys [{:name userid, :type 4} {:name entity, :type 4}]}
    {:name player_changename
     :eventid 30
     :keys
     [{:name userid, :type 4}
      {:name oldname, :type 1}
      {:name newname, :type 1}]}
    {:name player_hintmessage
     :eventid 31
     :keys [{:name hintmessage, :type 1}]}
    {:name game_init, :eventid 32}
    {:name game_newmap, :eventid 33, :keys [{:name mapname, :type 1}]}
    {:name game_start
     :eventid 34
     :keys
     [{:name roundslimit, :type 3}
      {:name timelimit, :type 3}
      {:name fraglimit, :type 3}
      {:name objective, :type 1}]}
    {:name game_end, :eventid 35, :keys [{:name winner, :type 5}]}
    {:name round_start
     :eventid 36
     :keys
     [{:name timelimit, :type 3}
      {:name fraglimit, :type 3}
      {:name objective, :type 1}]}
    {:name round_announce_match_point, :eventid 37}
    {:name round_announce_final, :eventid 38}
    {:name round_announce_last_round_half, :eventid 39}
    {:name round_announce_match_start, :eventid 40}
    {:name round_announce_warmup, :eventid 41}
    {:name round_end
     :eventid 42
     :keys
     [{:name winner, :type 5}
      {:name reason, :type 5}
      {:name message, :type 1}
      {:name legacy, :type 5}
      {:name player_count, :type 4}]}
    {:name round_end_upload_stats, :eventid 43}
    {:name round_officially_ended, :eventid 44}
    {:name ugc_map_info_received
     :eventid 45
     :keys [{:name published_file_id, :type 7}]}
    {:name ugc_map_unsubscribed
     :eventid 46
     :keys [{:name published_file_id, :type 7}]}
    {:name ugc_map_download_error
     :eventid 47
     :keys [{:name published_file_id, :type 7} {:name error_code, :type 3}]}
    {:name ugc_file_download_finished
     :eventid 48
     :keys [{:name hcontent, :type 7}]}
    {:name ugc_file_download_start
     :eventid 49
     :keys [{:name hcontent, :type 7} {:name published_file_id, :type 7}]}
    {:name begin_new_match, :eventid 50}
    {:name round_start_pre_entity, :eventid 51}
    {:name teamplay_round_start
     :eventid 52
     :keys [{:name full_reset, :type 6}]}
    {:name hostname_changed, :eventid 53, :keys [{:name hostname, :type 1}]}
    {:name difficulty_changed
     :eventid 54
     :keys
     [{:name newDifficulty, :type 4}
      {:name oldDifficulty, :type 4}
      {:name strDifficulty, :type 1}]}
    {:name finale_start, :eventid 55, :keys [{:name rushes, :type 4}]}
    {:name game_message
     :eventid 56
     :keys [{:name target, :type 5} {:name text, :type 1}]}
    {:name dm_bonus_weapon_start
     :eventid 57
     :keys [{:name time, :type 4} {:name wepID, :type 4} {:name Pos, :type 4}]}
    {:name survival_announce_phase, :eventid 58, :keys [{:name phase, :type 4}]}
    {:name break_breakable
     :eventid 59
     :keys
     [{:name entindex, :type 3}
      {:name userid, :type 4}
      {:name material, :type 5}]}
    {:name break_prop
     :eventid 60
     :keys [{:name entindex, :type 3} {:name userid, :type 4}]}
    {:name player_decal, :eventid 61, :keys [{:name userid, :type 4}]}
    {:name entity_killed
     :eventid 62
     :keys
     [{:name entindex_killed, :type 3}
      {:name entindex_attacker, :type 3}
      {:name entindex_inflictor, :type 3}
      {:name damagebits, :type 3}]}
    {:name bonus_updated
     :eventid 63
     :keys
     [{:name numadvanced, :type 4}
      {:name numbronze, :type 4}
      {:name numsilver, :type 4}
      {:name numgold, :type 4}]}
    {:name player_stats_updated
     :eventid 64
     :keys [{:name forceupload, :type 6}]}
    {:name achievement_event
     :eventid 65
     :keys
     [{:name achievement_name, :type 1}
      {:name cur_val, :type 4}
      {:name max_val, :type 4}]}
    {:name achievement_increment
     :eventid 66
     :keys
     [{:name achievement_id, :type 3}
      {:name cur_val, :type 4}
      {:name max_val, :type 4}]}
    {:name achievement_earned
     :eventid 67
     :keys [{:name player, :type 5} {:name achievement, :type 4}]}
    {:name achievement_write_failed, :eventid 68}
    {:name physgun_pickup, :eventid 69, :keys [{:name entindex, :type 3}]}
    {:name flare_ignite_npc, :eventid 70, :keys [{:name entindex, :type 3}]}
    {:name helicopter_grenade_punt_miss, :eventid 71}
    {:name user_data_downloaded, :eventid 72}
    {:name ragdoll_dissolved, :eventid 73, :keys [{:name entindex, :type 3}]}
    {:name gameinstructor_draw, :eventid 74}
    {:name gameinstructor_nodraw, :eventid 75}
    {:name map_transition, :eventid 76}
    {:name entity_visible
     :eventid 77
     :keys
     [{:name userid, :type 4}
      {:name subject, :type 4}
      {:name classname, :type 1}
      {:name entityname, :type 1}]}
    {:name set_instructor_group_enabled
     :eventid 78
     :keys [{:name group, :type 1} {:name enabled, :type 4}]}
    {:name instructor_server_hint_create
     :eventid 79
     :keys
     [{:name hint_name, :type 1}
      {:name hint_replace_key, :type 1}
      {:name hint_target, :type 3}
      {:name hint_activator_userid, :type 4}
      {:name hint_timeout, :type 4}
      {:name hint_icon_onscreen, :type 1}
      {:name hint_icon_offscreen, :type 1}
      {:name hint_caption, :type 1}
      {:name hint_activator_caption, :type 1}
      {:name hint_color, :type 1}
      {:name hint_icon_offset, :type 2}
      {:name hint_range, :type 2}
      {:name hint_flags, :type 3}
      {:name hint_binding, :type 1}
      {:name hint_gamepad_binding, :type 1}
      {:name hint_allow_nodraw_target, :type 6}
      {:name hint_nooffscreen, :type 6}
      {:name hint_forcecaption, :type 6}
      {:name hint_local_player_only, :type 6}]}
    {:name instructor_server_hint_stop
     :eventid 80
     :keys [{:name hint_name, :type 1}]}
    {:name read_game_titledata
     :eventid 81
     :keys [{:name controllerId, :type 4}]}
    {:name write_game_titledata
     :eventid 82
     :keys [{:name controllerId, :type 4}]}
    {:name reset_game_titledata
     :eventid 83
     :keys [{:name controllerId, :type 4}]}
    {:name weapon_reload_database, :eventid 84}
    {:name vote_ended, :eventid 85}
    {:name vote_started
     :eventid 86
     :keys
     [{:name issue, :type 1}
      {:name param1, :type 1}
      {:name team, :type 5}
      {:name initiator, :type 3}]}
    {:name vote_changed
     :eventid 87
     :keys
     [{:name vote_option1, :type 5}
      {:name vote_option2, :type 5}
      {:name vote_option3, :type 5}
      {:name vote_option4, :type 5}
      {:name vote_option5, :type 5}
      {:name potentialVotes, :type 5}]}
    {:name vote_passed
     :eventid 88
     :keys
     [{:name details, :type 1} {:name param1, :type 1} {:name team, :type 5}]}
    {:name vote_failed, :eventid 89, :keys [{:name team, :type 5}]}
    {:name vote_cast
     :eventid 90
     :keys
     [{:name vote_option, :type 5}
      {:name team, :type 4}
      {:name entityid, :type 3}]}
    {:name vote_options
     :eventid 91
     :keys
     [{:name count, :type 5}
      {:name option1, :type 1}
      {:name option2, :type 1}
      {:name option3, :type 1}
      {:name option4, :type 1}
      {:name option5, :type 1}]}
    {:name endmatch_mapvote_selecting_map
     :eventid 92
     :keys
     [{:name count, :type 5}
      {:name slot1, :type 5}
      {:name slot2, :type 5}
      {:name slot3, :type 5}
      {:name slot4, :type 5}
      {:name slot5, :type 5}
      {:name slot6, :type 5}
      {:name slot7, :type 5}
      {:name slot8, :type 5}
      {:name slot9, :type 5}
      {:name slot10, :type 5}]}
    {:name endmatch_cmm_start_reveal_items, :eventid 93}
    {:name inventory_updated, :eventid 94}
    {:name cart_updated, :eventid 95}
    {:name store_pricesheet_updated, :eventid 96}
    {:name gc_connected, :eventid 97}
    {:name item_schema_initialized, :eventid 98}
    {:name client_loadout_changed, :eventid 99}
    {:name add_player_sonar_icon
     :eventid 100
     :keys
     [{:name userid, :type 4}
      {:name pos_x, :type 2}
      {:name pos_y, :type 2}
      {:name pos_z, :type 2}]}
    {:name add_bullet_hit_marker
     :eventid 101
     :keys
     [{:name userid, :type 4}
      {:name bone, :type 4}
      {:name pos_x, :type 4}
      {:name pos_y, :type 4}
      {:name pos_z, :type 4}
      {:name ang_x, :type 4}
      {:name ang_y, :type 4}
      {:name ang_z, :type 4}
      {:name start_x, :type 4}
      {:name start_y, :type 4}
      {:name start_z, :type 4}
      {:name hit, :type 6}]}
    {:name verify_client_hit
     :eventid 102
     :keys
     [{:name userid, :type 4}
      {:name pos_x, :type 2}
      {:name pos_y, :type 2}
      {:name pos_z, :type 2}
      {:name timestamp, :type 2}]}
    {:name other_death
     :eventid 103
     :keys
     [{:name otherid, :type 4}
      {:name othertype, :type 1}
      {:name attacker, :type 4}
      {:name weapon, :type 1}
      {:name weapon_itemid, :type 1}
      {:name weapon_fauxitemid, :type 1}
      {:name weapon_originalowner_xuid, :type 1}
      {:name headshot, :type 6}
      {:name penetrated, :type 4}]}
    {:name item_purchase
     :eventid 104
     :keys
     [{:name userid, :type 4} {:name team, :type 4} {:name weapon, :type 1}]}
    {:name bomb_beginplant
     :eventid 105
     :keys [{:name userid, :type 4} {:name site, :type 4}]}
    {:name bomb_abortplant
     :eventid 106
     :keys [{:name userid, :type 4} {:name site, :type 4}]}
    {:name bomb_planted
     :eventid 107
     :keys [{:name userid, :type 4} {:name site, :type 4}]}
    {:name bomb_defused
     :eventid 108
     :keys [{:name userid, :type 4} {:name site, :type 4}]}
    {:name bomb_exploded
     :eventid 109
     :keys [{:name userid, :type 4} {:name site, :type 4}]}
    {:name bomb_dropped
     :eventid 110
     :keys [{:name userid, :type 4} {:name entindex, :type 3}]}
    {:name bomb_pickup, :eventid 111, :keys [{:name userid, :type 4}]}
    {:name defuser_dropped, :eventid 112, :keys [{:name entityid, :type 3}]}
    {:name defuser_pickup
     :eventid 113
     :keys [{:name entityid, :type 3} {:name userid, :type 4}]}
    {:name announce_phase_end, :eventid 114}
    {:name cs_intermission, :eventid 115}
    {:name bomb_begindefuse
     :eventid 116
     :keys [{:name userid, :type 4} {:name haskit, :type 6}]}
    {:name bomb_abortdefuse, :eventid 117, :keys [{:name userid, :type 4}]}
    {:name hostage_follows
     :eventid 118
     :keys [{:name userid, :type 4} {:name hostage, :type 4}]}
    {:name hostage_hurt
     :eventid 119
     :keys [{:name userid, :type 4} {:name hostage, :type 4}]}
    {:name hostage_killed
     :eventid 120
     :keys [{:name userid, :type 4} {:name hostage, :type 4}]}
    {:name hostage_rescued
     :eventid 121
     :keys
     [{:name userid, :type 4} {:name hostage, :type 4} {:name site, :type 4}]}
    {:name hostage_stops_following
     :eventid 122
     :keys [{:name userid, :type 4} {:name hostage, :type 4}]}
    {:name hostage_rescued_all, :eventid 123}
    {:name hostage_call_for_help
     :eventid 124
     :keys [{:name hostage, :type 4}]}
    {:name vip_escaped, :eventid 125, :keys [{:name userid, :type 4}]}
    {:name vip_killed
     :eventid 126
     :keys [{:name userid, :type 4} {:name attacker, :type 4}]}
    {:name player_radio
     :eventid 127
     :keys [{:name userid, :type 4} {:name slot, :type 4}]}
    {:name bomb_beep, :eventid 128, :keys [{:name entindex, :type 3}]}
    {:name weapon_fire
     :eventid 129
     :keys
     [{:name userid, :type 4}
      {:name weapon, :type 1}
      {:name silenced, :type 6}]}
    {:name weapon_fire_on_empty
     :eventid 130
     :keys [{:name userid, :type 4} {:name weapon, :type 1}]}
    {:name grenade_thrown
     :eventid 131
     :keys [{:name userid, :type 4} {:name weapon, :type 1}]}
    {:name weapon_outofammo, :eventid 132, :keys [{:name userid, :type 4}]}
    {:name weapon_reload, :eventid 133, :keys [{:name userid, :type 4}]}
    {:name weapon_zoom, :eventid 134, :keys [{:name userid, :type 4}]}
    {:name silencer_detach, :eventid 135, :keys [{:name userid, :type 4}]}
    {:name inspect_weapon, :eventid 136, :keys [{:name userid, :type 4}]}
    {:name weapon_zoom_rifle, :eventid 137, :keys [{:name userid, :type 4}]}
    {:name player_spawned
     :eventid 138
     :keys [{:name userid, :type 4} {:name inrestart, :type 6}]}
    {:name item_pickup
     :eventid 139
     :keys
     [{:name userid, :type 4} {:name item, :type 1} {:name silent, :type 6}]}
    {:name ammo_pickup
     :eventid 140
     :keys
     [{:name userid, :type 4} {:name item, :type 1} {:name index, :type 3}]}
    {:name item_equip
     :eventid 141
     :keys
     [{:name userid, :type 4}
      {:name item, :type 1}
      {:name canzoom, :type 6}
      {:name hassilencer, :type 6}
      {:name issilenced, :type 6}
      {:name hastracers, :type 6}
      {:name weptype, :type 4}
      {:name ispainted, :type 6}]}
    {:name enter_buyzone
     :eventid 142
     :keys [{:name userid, :type 4} {:name canbuy, :type 6}]}
    {:name exit_buyzone
     :eventid 143
     :keys [{:name userid, :type 4} {:name canbuy, :type 6}]}
    {:name buytime_ended, :eventid 144}
    {:name enter_bombzone
     :eventid 145
     :keys
     [{:name userid, :type 4}
      {:name hasbomb, :type 6}
      {:name isplanted, :type 6}]}
    {:name exit_bombzone
     :eventid 146
     :keys
     [{:name userid, :type 4}
      {:name hasbomb, :type 6}
      {:name isplanted, :type 6}]}
    {:name enter_rescue_zone, :eventid 147, :keys [{:name userid, :type 4}]}
    {:name exit_rescue_zone, :eventid 148, :keys [{:name userid, :type 4}]}
    {:name silencer_off, :eventid 149, :keys [{:name userid, :type 4}]}
    {:name silencer_on, :eventid 150, :keys [{:name userid, :type 4}]}
    {:name buymenu_open, :eventid 151, :keys [{:name userid, :type 4}]}
    {:name buymenu_close, :eventid 152, :keys [{:name userid, :type 4}]}
    {:name round_prestart, :eventid 153}
    {:name round_poststart, :eventid 154}
    {:name grenade_bounce, :eventid 155, :keys [{:name userid, :type 4}]}
    {:name hegrenade_detonate
     :eventid 156
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name flashbang_detonate
     :eventid 157
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name smokegrenade_detonate
     :eventid 158
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name smokegrenade_expired
     :eventid 159
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name molotov_detonate
     :eventid 160
     :keys
     [{:name userid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name decoy_detonate
     :eventid 161
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name decoy_started
     :eventid 162
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name tagrenade_detonate
     :eventid 163
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name inferno_startburn
     :eventid 164
     :keys
     [{:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name inferno_expire
     :eventid 165
     :keys
     [{:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name inferno_extinguish
     :eventid 166
     :keys
     [{:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name decoy_firing
     :eventid 167
     :keys
     [{:name userid, :type 4}
      {:name entityid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name bullet_impact
     :eventid 168
     :keys
     [{:name userid, :type 4}
      {:name x, :type 2}
      {:name y, :type 2}
      {:name z, :type 2}]}
    {:name player_footstep, :eventid 169, :keys [{:name userid, :type 4}]}
    {:name player_jump, :eventid 170, :keys [{:name userid, :type 4}]}
    {:name player_blind
     :eventid 171
     :keys
     [{:name userid, :type 4}
      {:name attacker, :type 4}
      {:name entityid, :type 4}
      {:name blind_duration, :type 2}]}
    {:name player_falldamage
     :eventid 172
     :keys [{:name userid, :type 4} {:name damage, :type 2}]}
    {:name door_moving
     :eventid 173
     :keys [{:name entindex, :type 3} {:name userid, :type 4}]}
    {:name round_freeze_end, :eventid 174}
    {:name mb_input_lock_success, :eventid 175}
    {:name mb_input_lock_cancel, :eventid 176}
    {:name nav_blocked
     :eventid 177
     :keys [{:name area, :type 3} {:name blocked, :type 6}]}
    {:name nav_generate, :eventid 178}
    {:name achievement_info_loaded, :eventid 179}
    {:name spec_target_updated, :eventid 180, :keys [{:name userid, :type 5}]}
    {:name spec_mode_updated, :eventid 181, :keys [{:name userid, :type 5}]}
    {:name hltv_changed_mode
     :eventid 182
     :keys
     [{:name oldmode, :type 3}
      {:name newmode, :type 3}
      {:name obs_target, :type 3}]}
    {:name cs_game_disconnected, :eventid 183}
    {:name cs_win_panel_round
     :eventid 184
     :keys
     [{:name show_timer_defend, :type 6}
      {:name show_timer_attack, :type 6}
      {:name timer_time, :type 4}
      {:name final_event, :type 5}
      {:name funfact_token, :type 1}
      {:name funfact_player, :type 4}
      {:name funfact_data1, :type 3}
      {:name funfact_data2, :type 3}
      {:name funfact_data3, :type 3}]}
    {:name cs_win_panel_match, :eventid 185}
    {:name cs_match_end_restart, :eventid 186}
    {:name cs_pre_restart, :eventid 187}
    {:name show_freezepanel
     :eventid 188
     :keys
     [{:name victim, :type 4}
      {:name killer, :type 4}
      {:name hits_taken, :type 4}
      {:name damage_taken, :type 4}
      {:name hits_given, :type 4}
      {:name damage_given, :type 4}]}
    {:name hide_freezepanel, :eventid 189}
    {:name freezecam_started, :eventid 190}
    {:name player_avenged_teammate
     :eventid 191
     :keys [{:name avenger_id, :type 4} {:name avenged_player_id, :type 4}]}
    {:name achievement_earned_local
     :eventid 192
     :keys [{:name achievement, :type 4} {:name splitscreenplayer, :type 4}]}
    {:name item_found
     :eventid 193
     :keys
     [{:name player, :type 5}
      {:name quality, :type 5}
      {:name method, :type 5}
      {:name itemdef, :type 3}
      {:name itemid, :type 3}]}
    {:name repost_xbox_achievements
     :eventid 194
     :keys [{:name splitscreenplayer, :type 4}]}
    {:name match_end_conditions
     :eventid 195
     :keys
     [{:name frags, :type 3}
      {:name max_rounds, :type 3}
      {:name win_rounds, :type 3}
      {:name time, :type 3}]}
    {:name round_mvp
     :eventid 196
     :keys
     [{:name userid, :type 4}
      {:name reason, :type 4}
      {:name musickitmvps, :type 3}]}
    {:name client_disconnect, :eventid 197}
    {:name gg_player_levelup
     :eventid 198
     :keys
     [{:name userid, :type 4}
      {:name weaponrank, :type 4}
      {:name weaponname, :type 1}]}
    {:name ggtr_player_levelup
     :eventid 199
     :keys
     [{:name userid, :type 4}
      {:name weaponrank, :type 4}
      {:name weaponname, :type 1}]}
    {:name assassination_target_killed
     :eventid 200
     :keys [{:name target, :type 4} {:name killer, :type 4}]}
    {:name ggprogressive_player_levelup
     :eventid 201
     :keys
     [{:name userid, :type 4}
      {:name weaponrank, :type 4}
      {:name weaponname, :type 1}]}
    {:name gg_killed_enemy
     :eventid 202
     :keys
     [{:name victimid, :type 4}
      {:name attackerid, :type 4}
      {:name dominated, :type 4}
      {:name revenge, :type 4}
      {:name bonus, :type 6}]}
    {:name gg_final_weapon_achieved
     :eventid 203
     :keys [{:name playerid, :type 4}]}
    {:name gg_bonus_grenade_achieved
     :eventid 204
     :keys [{:name userid, :type 4}]}
    {:name switch_team
     :eventid 205
     :keys
     [{:name numPlayers, :type 4}
      {:name numSpectators, :type 4}
      {:name avg_rank, :type 4}
      {:name numTSlotsFree, :type 4}
      {:name numCTSlotsFree, :type 4}]}
    {:name gg_leader, :eventid 206, :keys [{:name playerid, :type 4}]}
    {:name gg_team_leader, :eventid 207, :keys [{:name playerid, :type 4}]}
    {:name gg_player_impending_upgrade
     :eventid 208
     :keys [{:name userid, :type 4}]}
    {:name write_profile_data, :eventid 209}
    {:name trial_time_expired, :eventid 210, :keys [{:name slot, :type 4}]}
    {:name update_matchmaking_stats, :eventid 211}
    {:name player_reset_vote
     :eventid 212
     :keys [{:name userid, :type 4} {:name vote, :type 6}]}
    {:name enable_restart_voting, :eventid 213, :keys [{:name enable, :type 6}]}
    {:name sfuievent
     :eventid 214
     :keys
     [{:name action, :type 1} {:name data, :type 1} {:name slot, :type 5}]}
    {:name start_vote
     :eventid 215
     :keys
     [{:name userid, :type 4}
      {:name type, :type 5}
      {:name vote_parameter, :type 4}]}
    {:name player_given_c4, :eventid 216, :keys [{:name userid, :type 4}]}
    {:name gg_reset_round_start_sounds
     :eventid 217
     :keys [{:name userid, :type 4}]}
    {:name tr_player_flashbanged, :eventid 218, :keys [{:name userid, :type 4}]}
    {:name tr_mark_complete, :eventid 219, :keys [{:name complete, :type 4}]}
    {:name tr_mark_best_time, :eventid 220, :keys [{:name time, :type 3}]}
    {:name tr_exit_hint_trigger, :eventid 221}
    {:name bot_takeover
     :eventid 222
     :keys
     [{:name userid, :type 4} {:name botid, :type 4} {:name index, :type 4}]}
    {:name tr_show_finish_msgbox, :eventid 223, :keys [{:name userid, :type 4}]}
    {:name tr_show_exit_msgbox, :eventid 224, :keys [{:name userid, :type 4}]}
    {:name reset_player_controls, :eventid 225}
    {:name jointeam_failed
     :eventid 226
     :keys [{:name userid, :type 4} {:name reason, :type 5}]}
    {:name teamchange_pending
     :eventid 227
     :keys [{:name userid, :type 4} {:name toteam, :type 5}]}
    {:name material_default_complete, :eventid 228}
    {:name cs_prev_next_spectator, :eventid 229, :keys [{:name next, :type 6}]}
    {:name nextlevel_changed, :eventid 231, :keys [{:name nextlevel, :type 1}]}
    {:name seasoncoin_levelup
     :eventid 232
     :keys
     [{:name player, :type 4} {:name category, :type 4} {:name rank, :type 4}]}
    {:name tournament_reward
     :eventid 233
     :keys
     [{:name defindex, :type 3}
      {:name totalrewards, :type 3}
      {:name accountid, :type 3}]}
    {:name start_halftime, :eventid 234}
    {:name hltv_status
     :eventid 235
     :keys
     [{:name clients, :type 3}
      {:name slots, :type 3}
      {:name proxies, :type 4}
      {:name master, :type 1}
      {:name externaltotal, :type 3}
      {:name externallinked, :type 3}]}
    {:name hltv_cameraman, :eventid 236, :keys [{:name index, :type 4}]}
    {:name hltv_rank_camera
     :eventid 237
     :keys
     [{:name index, :type 5} {:name rank, :type 2} {:name target, :type 4}]}
    {:name hltv_rank_entity
     :eventid 238
     :keys
     [{:name index, :type 4} {:name rank, :type 2} {:name target, :type 4}]}
    {:name hltv_fixed
     :eventid 239
     :keys
     [{:name posx, :type 3}
      {:name posy, :type 3}
      {:name posz, :type 3}
      {:name theta, :type 4}
      {:name phi, :type 4}
      {:name offset, :type 4}
      {:name fov, :type 2}
      {:name target, :type 4}]}
    {:name hltv_chase
     :eventid 240
     :keys
     [{:name target1, :type 4}
      {:name target2, :type 4}
      {:name distance, :type 4}
      {:name theta, :type 4}
      {:name phi, :type 4}
      {:name inertia, :type 5}
      {:name ineye, :type 5}]}
    {:name hltv_message, :eventid 241, :keys [{:name text, :type 1}]}
    {:name hltv_title, :eventid 242, :keys [{:name text, :type 1}]}
    {:name hltv_chat, :eventid 243, :keys [{:name text, :type 1}]}
    {:name hltv_changed_target
     :eventid 244
     :keys
     [{:name mode, :type 3}
      {:name old_target, :type 3}
      {:name obs_target, :type 3}]}]})
