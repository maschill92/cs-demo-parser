(ns cs-demo-parser.core
  (:require [clojure.java.io :as io]
            [cs-demo-parser.spec :as spec]
            [cs-demo-parser.protobuf :as proto]
            [flatland.protobuf.core :as protobuf]
            [octet.core :as buf])
  (:import [org.apache.commons.io IOUtils]))

(def unknown-msg-count (atom 0))

(def packet-cmd-ids {0 [:nop proto/NOP]
                     1 [:disconnect proto/Disconnect]
                     2 [:file proto/File]
                     4 [:tick proto/Tick]
                     5 [:string-cmd proto/StringCmd]
                     6 [:set-con-var proto/SetConVar]
                     7 [:signon-state proto/SignonState]
                     8 [:server-info proto/ServerInfo]
                     9 [:send-table proto/SendTable]
                     10 [:class-info proto/ClassInfo]
                     11 [:set-pause proto/SetPause]
                     12 [:create-string-table proto/CreateStringTable]
                     13 [:update-string-table proto/UpdateStringTable]
                     14 [:voice-init proto/VoiceInit]
                     15 [:voice-data proto/VoiceData]
                     16 [:print proto/Print]
                     17 [:sounds proto/Sounds]
                     18 [:set-view proto/SetView]
                     19 [:fix-angle proto/FixAngle]
                     20 [:crosshair-angle proto/CrosshairAngle]
                     21 [:bsp-decal proto/BSPDecal]
                     23 [:user-message proto/UserMessage]
                     25 [:game-event proto/GameEvent]
                     26 [:packet-entities proto/PacketEntities]
                     27 [:temp-entities proto/TempEntities]
                     28 [:prefetch proto/Prefetch]
                     29 [:menu proto/Menu]
                     30 [:game-event-list proto/GameEventList]
                     31 [:get-cvar-value proto/GetCvarValue]})

(def frame-cmds {1 :signon
                 2 :packet
                 3 :synctick
                 4 :console-cmd
                 5 :user-cmd
                 6 :data-tables
                 7 :stop
                 8 :custom-data
                 9 :string-tables})

(defn- parse-spec [input-stream spec]
  (let [buffer (buf/allocate (buf/size spec))]
    (.read input-stream (.array buffer))
    (buf/read buffer spec)))

(defn- parse-demo-header [input-stream] (parse-spec input-stream spec/demo-header))

(defn- parse-cmd-header [input-stream] (parse-spec input-stream spec/cmd-header))

(defn- skip-raw-data [input-stream]
  (let [size-buffer (buf/allocate (buf/size spec/int32-le))]
    (.read input-stream (.array size-buffer))
    (let [num-bytes-to-skip (long (buf/read size-buffer spec/int32-le))]
      (IOUtils/skip input-stream num-bytes-to-skip))))

(defn- parse-cmd-info [input-stream] (parse-spec input-stream spec/cmd-info))

(defn- parse-sequence-info [input-stream] (parse-spec input-stream spec/sequence-info))

(defn- parse-packet-size [input-stream] (parse-spec input-stream spec/int32-le))

(defn parse-data-tables [input-stream callback-fns]
  (let [total-data-table-size (parse-packet-size input-stream)
        data-table-callback-fn (:data-table callback-fns)]
    (if data-table-callback-fn
      (loop [num-bytes-read 0]
        (if (> total-data-table-size num-bytes-read)
          (let [[bytes-read-for-type type] (spec/read-var-int32 input-stream)
                [bytes-read-for-size size] (spec/read-var-int32 input-stream)
                num-bytes-read (+ num-bytes-read size bytes-read-for-type bytes-read-for-size)]
            (let [buffer (buf/allocate size)]
              (.read input-stream (.array buffer))
              (let [data-table (protobuf/protobuf-load proto/SendTable (.array buffer))]
                (data-table-callback-fn data-table callback-fns)
                (if (not (:is-end data-table))
                  (recur num-bytes-read)
                  (IOUtils/skip input-stream (- total-data-table-size num-bytes-read))))))))
      (IOUtils/skip input-stream total-data-table-size))))

(defn- parse-packet-data [input-stream callback-fns]
  (let [total-packet-size (parse-packet-size input-stream)]
    (loop [num-bytes-read 0]
      (if (> total-packet-size num-bytes-read)
        (let [[bytes-read-for-cmd packet-cmd] (spec/read-var-int32 input-stream)
              [bytes-read-for-size packet-size] (spec/read-var-int32 input-stream)
              num-bytes-read (+ num-bytes-read bytes-read-for-cmd bytes-read-for-size packet-size)
              [packet-cmd-name packet-cmd-protodef] (get packet-cmd-ids packet-cmd)
              packet-cmd-callback-fn (get-in callback-fns [:packet-cmd packet-cmd-name])]
          (if (and packet-cmd-protodef packet-cmd-callback-fn)
            (let [buffer (buf/allocate packet-size)]
              (.read input-stream (.array buffer))
              (packet-cmd-callback-fn (protobuf/protobuf-load packet-cmd-protodef (.array buffer)) callback-fns))
            (do
              (swap! unknown-msg-count inc)
              (IOUtils/skip input-stream packet-size)))
          (recur num-bytes-read))))))

(defn- parse-packet [input-stream callback-fns]
  (parse-cmd-info input-stream)
  (parse-sequence-info input-stream)
  (parse-packet-data input-stream callback-fns))

(defn- parse-demo-frames [input-stream callback-fns]
  (loop []
    (let [cmd-header (parse-cmd-header input-stream)
          frame-cmd (get frame-cmds (:cmd cmd-header))]
      (cond
        (= :stop frame-cmd) nil
        (= :synctick frame-cmd) (recur)
        (or (= :signon frame-cmd)
            (= :packet frame-cmd)) (do
                                     (parse-packet input-stream callback-fns)
                                     (recur))
        (= :data-tables frame-cmd) (do
                                     (parse-data-tables input-stream callback-fns)
                                     (recur))
        (or (= :console-cmd frame-cmd)
            (= :string-tables frame-cmd)) (do
                                            (skip-raw-data input-stream)
                                            (recur))
        :else (throw (RuntimeException. (str "breh... " cmd-header)))))))

(defn- parse-game-event-list [game-event-list-proto]
  (reduce #(assoc %1 (:eventid %2) %2) {} (:descriptors game-event-list-proto)))
(defn create-game-event-list-handler [game-event-list-atom]
  (fn [game-event-list-proto callback-fns]
    (swap! game-event-list-atom merge (parse-game-event-list game-event-list-proto))))

;; source engine game event types
(def event-types {1 :val-string
                  2 :val-float
                  3 :val-long
                  4 :val-short
                  5 :val-byte
                  6 :val-bool
                  7 :val-wstring})
(defn parse-game-event [game-event-proto game-event-list]
  (if-let [game-event-info (get game-event-list (:eventid game-event-proto))]
    (let [fields (map vector (:keys game-event-info) (:keys game-event-proto))
          base-game-event {:eventid (:eventid game-event-proto) :name (:name game-event-info) :data {}}]
      (reduce (fn [game-event [{:keys [type name]} val]]
                (assoc-in game-event [:data name] (get val (get event-types type)))) base-game-event fields))
    (throw (RuntimeException. "Unknown event " (:eventid game-event-proto)))))
(defn handle-game-event [game-event callback-fns]
  (if-let [game-event-callback-fn (get-in callback-fns [:game-event (:name game-event)])]
    (game-event-callback-fn game-event)))
(defn create-game-event-handler [game-event-list-atom]
  (fn [game-event-proto callback-fns]
    (handle-game-event (parse-game-event game-event-proto @game-event-list-atom) callback-fns)))

(defn create-data-tables-handler [data-tables-atom]
  (fn [data-table callback-fns]
    (println (:net-table-name data-table))))

(defn handle-packet-cmd [packet-cmd callback-fns]
  (println packet-cmd))

(defn parse-demo
  ([] (let [game-event-list-atom (atom {})
            data-tables-atom (atom {})]
        (parse-demo {:packet-cmd {:create-string-table handle-packet-cmd
                                  :update-string-table handle-packet-cmd}})))

  ([{demo-header-fn :demo-header :as callback-fns}]
   (with-open [input-stream (io/input-stream (io/resource "demo.dem"))]
     (let [demo-header (parse-demo-header input-stream)]
       (if demo-header-fn (demo-header-fn demo-header)))
     (parse-demo-frames input-stream callback-fns))
   unknown-msg-count))
