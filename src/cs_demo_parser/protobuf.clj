(ns cs-demo-parser.protobuf
  (:require [flatland.protobuf.core :as proto])
  (:import (com.valve NetmessagesPublic$CNETMsg_NOP
                      NetmessagesPublic$CNETMsg_Disconnect
                      NetmessagesPublic$CNETMsg_File
                      NetmessagesPublic$CNETMsg_Tick
                      NetmessagesPublic$CNETMsg_StringCmd
                      NetmessagesPublic$CNETMsg_SetConVar
                      NetmessagesPublic$CNETMsg_SignonState
                      NetmessagesPublic$CSVCMsg_ServerInfo
                      NetmessagesPublic$CSVCMsg_SendTable
                      NetmessagesPublic$CSVCMsg_ClassInfo
                      NetmessagesPublic$CSVCMsg_SetPause
                      NetmessagesPublic$CSVCMsg_CreateStringTable
                      NetmessagesPublic$CSVCMsg_UpdateStringTable
                      NetmessagesPublic$CSVCMsg_VoiceInit
                      NetmessagesPublic$CSVCMsg_VoiceData
                      NetmessagesPublic$CSVCMsg_Print
                      NetmessagesPublic$CSVCMsg_Sounds
                      NetmessagesPublic$CSVCMsg_SetView
                      NetmessagesPublic$CSVCMsg_FixAngle
                      NetmessagesPublic$CSVCMsg_CrosshairAngle
                      NetmessagesPublic$CSVCMsg_BSPDecal
                      NetmessagesPublic$CSVCMsg_UserMessage
                      NetmessagesPublic$CSVCMsg_GameEvent
                      NetmessagesPublic$CSVCMsg_PacketEntities
                      NetmessagesPublic$CSVCMsg_TempEntities
                      NetmessagesPublic$CSVCMsg_Prefetch
                      NetmessagesPublic$CSVCMsg_Menu
                      NetmessagesPublic$CSVCMsg_GameEventList
                      NetmessagesPublic$CSVCMsg_GetCvarValue
                      NetmessagesPublic$CSVCMsg_SendTable)))

(def NOP (proto/protodef NetmessagesPublic$CNETMsg_NOP))
(def Disconnect (proto/protodef NetmessagesPublic$CNETMsg_Disconnect))
(def File (proto/protodef NetmessagesPublic$CNETMsg_File))
(def Tick (proto/protodef NetmessagesPublic$CNETMsg_Tick))
(def StringCmd (proto/protodef NetmessagesPublic$CNETMsg_StringCmd))
(def SetConVar (proto/protodef NetmessagesPublic$CNETMsg_SetConVar))
(def SignonState (proto/protodef NetmessagesPublic$CNETMsg_SignonState))
(def ServerInfo (proto/protodef NetmessagesPublic$CSVCMsg_ServerInfo))
(def SendTable (proto/protodef NetmessagesPublic$CSVCMsg_SendTable))
(def ClassInfo (proto/protodef NetmessagesPublic$CSVCMsg_ClassInfo))
(def SetPause (proto/protodef NetmessagesPublic$CSVCMsg_SetPause))
(def CreateStringTable (proto/protodef NetmessagesPublic$CSVCMsg_CreateStringTable))
(def UpdateStringTable (proto/protodef NetmessagesPublic$CSVCMsg_UpdateStringTable))
(def VoiceInit (proto/protodef NetmessagesPublic$CSVCMsg_VoiceInit))
(def VoiceData (proto/protodef NetmessagesPublic$CSVCMsg_VoiceData))
(def Print (proto/protodef NetmessagesPublic$CSVCMsg_Print))
(def Sounds (proto/protodef NetmessagesPublic$CSVCMsg_Sounds))
(def SetView (proto/protodef NetmessagesPublic$CSVCMsg_SetView))
(def FixAngle (proto/protodef NetmessagesPublic$CSVCMsg_FixAngle))
(def CrosshairAngle (proto/protodef NetmessagesPublic$CSVCMsg_CrosshairAngle))
(def BSPDecal (proto/protodef NetmessagesPublic$CSVCMsg_BSPDecal))
(def UserMessage (proto/protodef NetmessagesPublic$CSVCMsg_UserMessage))
(def GameEvent (proto/protodef NetmessagesPublic$CSVCMsg_GameEvent))
(def PacketEntities (proto/protodef NetmessagesPublic$CSVCMsg_PacketEntities))
(def TempEntities (proto/protodef NetmessagesPublic$CSVCMsg_TempEntities))
(def Prefetch (proto/protodef NetmessagesPublic$CSVCMsg_Prefetch))
(def Menu (proto/protodef NetmessagesPublic$CSVCMsg_Menu))
(def GameEventList (proto/protodef NetmessagesPublic$CSVCMsg_GameEventList))
(def GetCvarValue (proto/protodef NetmessagesPublic$CSVCMsg_GetCvarValue))

(def SendTable (proto/protodef NetmessagesPublic$CSVCMsg_SendTable))
