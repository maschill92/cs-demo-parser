(ns cs-demo-parser.spec
  (:require [octet.core :as buf]
            [octet.buffer :as buffer]
            [octet.spec.basic :as basic-spec]
            [octet.spec :as spec]))
;; Read protocol buffer variant int32
(defn read-var-int32 [input-stream]
  (loop [count 0
         result 0]
    (let [b (.read input-stream)
          new-result (bit-or result (bit-shift-left (bit-and b 0x7F) (* 7 count)))
          num-bytes-read (inc count)]
      (if (zero? (bit-and b 0x80))
        [num-bytes-read new-result]
        (recur num-bytes-read new-result)))))

;; Define custom little-endian int32 type
(defn- read-int32-le [buff pos]
  (Integer/reverseBytes (buffer/read-int buff pos)))
(defn- write-int32-le [buff pos value]
  (buffer/write-int buff pos (Integer/reverseBytes value)))
(def int32-le
  (basic-spec/primitive-spec 4 read-int32-le write-int32-le))

(def demo-header (buf/spec :header-id (buf/string 8)
                           :demo-protocol int32-le
                           :network-protocol int32-le
                           :server-name (buf/string 260)
                           :client-name (buf/string 260)
                           :map-name (buf/string 260)
                           :game-directory (buf/string 260)
                           :playback-time buf/float
                           :playback-ticks int32-le
                           :playback-frames int32-le
                           :signon-length int32-le))

(def cmd-header (buf/spec :cmd buf/byte
                          :server-tick int32-le
                          :player-slot buf/byte))
;; CommandInfo
(def cs-vector (buf/repeat 3 buf/float))
(def split (buf/spec :flags buf/int32
                      :view-origin cs-vector
                      :view-angle cs-vector
                      :local-view-angle cs-vector
                      :view-origin2 cs-vector
                      :view-angle2 cs-vector
                      :local-view-angle2 cs-vector))
(def cmd-info (buf/spec :p1 split
                        :p2 split))
;; SequenceInfo
(def sequence-info (buf/spec :seq-in int32-le
                             :seq-out int32-le))
